# Snoot
The need for a timely response is critical to the continuous feedback loop within active learning. Automated grading systems help address this need by giving students near instant feedback on their programming assignment submissions. As students write and submit their programs, they receive feedback from the automated grading system so that they can improve their next submission.

As students converge to a solution, it becomes easier to synthesize repairs that will result in a passing solution. Rather than offering these repairs to students as a gimme, Snoot provides feedback based on these repairs. Operating over a general-purpose programming language, [Colang](https://bitbucket.org/ctnelson1997/colang/src/master/), Snoot tests the boundary cases of a student submission to sniff out program repairs and offer appropriate feedback. From this, students will be able to make use of the feedback before making a final submission.

Video Demonstration: https://www.youtube.com/watch?v=Ke6HtIhXv8Y

## Snoot-Frontend

This responsive interface is written using React and TypeScript. Users can specify from a list of environments, `PerformFactorial` and `PrintStarcase` where they can practice the synthesis and evaluation techniques provided by Snoot and Colang. While these two environments are the only ones available, the program is written to be extensible to more.

Students can write their programs on the left textarea, and either submit them to undergo Colang evaluation, or request feedback to undergo Colang synthetization. These requests are sent to Snoot-Backend to handle, which interfaces with the Colang language. Note that programs that exceed 60 seconds or 9999 iterations will be automatically timed out.

The frontend interfaces with backend, making `POST` requests to begin an evaluation or a synthetization, and `GET` requests to check on their status.

By default, program repairs are not visible to the student. Instead, they need to utilize the schnozzes hinting at a line number to determine which boundary cases their program fails. The student must enter `snoot` at the prompt to gain access to the repaired program.

## Snoot-Backend

This API service is written using Node, Express, and TypeScript. It has several endpoints which interface Snoot-Frontend and the Colang language...

### Evaluation

`POST /colang/api/evaluate`

Requires a body of the form...

```json
{
    "content": "{program_content}",
    "program": "{program_name}"
}
```

... where `program_content` is the code of the Colang program, and `program_name` is the name of the environment, currently either `factorial` or `starcase`. The server begins the evaluation of the program, and returns a token in the body such as...

``````json
{
    "token":{token_number}
}
``````

... where `{token_number}` can be used in all future GET requests to get the current status of the evaluation.

`GET /colang/api/evaluate/status?token={token_number}`

Gets the current status of an evaluation mapped to `token_number`. Returns a body of the form...

```json
{
    "ready":{ready_status},
    "correct":{evaluation_status},
    "date":"{date_of_evaluation}"
}
```

... where `ready_status` is a boolean of whether or not the evaluation has been completed, `evaluation_status` is a boolean of whether or not the given `program_content` passed the set of test cases, and `date_of_evaluation` is the ISO string of when the program was evaluated.

### Synthetization

`POST /colang/api/synthesize`

Requires a body of the form...

```json
{
    "content": "{program_content}",
    "program": "{program_name}"
}
```

... where `program_content` is the code of the Colang program, and `program_name` is the name of the environment, currently either `factorial` or `starcase`. The server begins the synthetization of the program, and returns a token in the body such as...

``````json
{
    "token":{token_number}
}
``````

... where `{token_number}` can be used in all future GET requests to get the current status of the synthetization.

`GET /colang/api/synthesize/status?token={token_number}`

Gets the current status of a synthetization mapped to `token_number`. Returns a body of the form...

```json
{
    "ready":{ready_status},
    "solved":{solved_status},
    "date":"{date_of_evaluation}",
    "repairs":"{program_repairs}",
    "snoot1Exists":{snoot1_exists},
    "snoot2Exists":{snoot2_exists},
    "snoot1":{snoot1_line_number},
    "snoot2":{snoot2_line_number}
}
```

... where `ready_status` is a boolean of whether or not the synthetization has been completed, `solved_status` is a boolean of whether or not the given `program_content` was able to be repaired, and `date_of_evaluation` is the ISO string of when the program was evaluated, `program_repairs` is the repaired program, `snoot1_exists` is a boolean stating whether or not there is at least one line of code repaired, `snoot2_exists` is a boolean stating whether or not there are two lines of code repaired, `snoot1_line_number` is the line number in which the first repair occurred, and `snoot2_line_number` is the line number in which the second repair occurred.

### Storage

All student submissions are held within `./filesys/submissions/` of the Snoot-Backend. In particular, the student code can be found within `./filesys/submissions/{submission_folder}/in/ColangProgram.colang`, the results (if any) in `./filesys/submissions/{submission_folder}/out/` and any auxiliary files in `./filesys/submissions/{submission_folder}/work/`. These can be used for future research and work.

## Docker

For your convenience, Snoot is available as a Docker image built upon the Colang Docker image from `ctnelson1997/snoot:latest`. You can execute the following commands to start it on your own machine.

`docker pull ctnelson1997/snoot:latest`

`docker run -td -p 53706:53706 -p 53705:53705 --name snoot ctnelson1997/snoot:latest sh entrypoint.sh`

**Note**: If the container is running or has been ran before you may need to execute `docker stop snoot && docker rm snoot`.

**Note**: The above will run the container in detached mode. If you need to access the logs for the frontend, you can do so by executing `docker logs snoot`. If you need to access the logs from the backend, you will have to enter the container using `docker exec -it --user="root" snoot /bin/sh` and run command `screen -r snoot_backend`. To exit, you can press `ctrl + a + d` followed by `exit` to return to the main terminal.

### Running Snoot

After running the above `docker run ` command, to open the web interface, use a web browser and access `http://localhost:53706/`. **Note:** If your docker container is operating on another IP address, you will need specify that IP address in place of `localhost`. In addition, you will have to specify that IP on port `:53705` for the backend address through the Snoot interface.