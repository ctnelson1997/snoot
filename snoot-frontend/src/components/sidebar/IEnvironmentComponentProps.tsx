export default interface IEnvironmentComponentProps {
    isCollapsed: boolean,
    getEnvironment: any,
    setEnvironment: any
}