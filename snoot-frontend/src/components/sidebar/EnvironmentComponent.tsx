import React from 'react'
import './EnvironmentComponent.css';
import IEnvironmentComponentProps from './IEnvironmentComponentProps';
import IEnvironmentComponentState from './IEnvironmentComponentState';
import Card from 'react-bootstrap/Card';
import CardDeck from 'react-bootstrap/CardDeck';
import { Button } from 'react-bootstrap';

export default class EnvironmentComponent extends React.Component<IEnvironmentComponentProps, IEnvironmentComponentState> {

    select(environment: string) {
        this.props.setEnvironment(environment);
    }

    getButton(environment: string) {
        if (this.props.getEnvironment() !== environment) {
            return (
                <Button variant="primary" type="submit" className="buttonSpacing" onClick={() => this.select(environment)}>
                    Select
                </Button>
            );
        } else {
            return (
                <Button variant="secondary" type="submit" className="buttonSpacing" onClick={() => this.select(environment)}>
                    Selected
                </Button>
            );
        }
    }

    getCards() {
        return (
            <>
                <CardDeck style={{ display: 'flex', flexDirection: 'row' }}>
                    <Card
                        bg={"light"}
                        text={'dark'}
                        className="mb-2"
                        style={{ flex: 1 }}
                    >
                        <Card.Header style={{ fontWeight: 600 }}>PerformFactorial.colang</Card.Header>

                        <Card.Body>
                            <Card.Text>
                                Compute the factorial of n.
                        </Card.Text>
                        {this.getButton("factorial")}
                        </Card.Body>
                    </Card>
                    <Card
                        bg={"light"}
                        text={'dark'}
                        className="mb-2"
                        style={{ flex: 1, width: "50rem" }}
                    >
                        <Card.Header style={{ fontWeight: 600 }}>PrintStarcase.colang</Card.Header>

                        <Card.Body>
                            <Card.Text>
                                Display a staircase of n stars.
                        </Card.Text>
                        {this.getButton("starcase")}
                        </Card.Body>
                    </Card>
                </CardDeck>
                <div style={{ marginBottom: "2rem" }}></div>
            </>
        )
    }
    render() {
        return <div>
            {
                this.props.isCollapsed ? <></> : this.getCards()
            }
        </div>
    }
}