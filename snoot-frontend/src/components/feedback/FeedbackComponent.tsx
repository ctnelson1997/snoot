import './FeedbackComponent.css';

import React from 'react'
import IFeedbackComponentProps from './IFeedbackComponentProps';
import { Spinner, Form, Button } from 'react-bootstrap';
import IFeedbackComponentState from './IFeedbackComponentState';

const ADMIN_PASSWORD = "snoot";

export default class FeedbackComponent extends React.Component<IFeedbackComponentProps, IFeedbackComponentState> {

    constructor(props: IFeedbackComponentProps | Readonly<IFeedbackComponentProps>) {
        super(props);
        this.state = {
            intervalChecker: undefined,
            localFeedbackToken: -1,
            feedbackRepairs: "",
            isShowingRepairs: false,
            isLoading: false,
            hasSnoot1: false,
            hasSnoot2: false,
            snoot1: 0,
            snoot2: 0
        }

        this.checkForFeedback = this.checkForFeedback.bind(this);
        this.showRepairs = this.showRepairs.bind(this);
    }

    componentDidUpdate() {
        if (this.props.getFeedbackToken() !== this.state.localFeedbackToken && this.props.getFeedbackToken() !== -1) {
            this.setState({
                isLoading: true,
                localFeedbackToken: this.props.getFeedbackToken()
            },
                () => {
                    if (this.state.intervalChecker) {
                        clearInterval(this.state.intervalChecker);
                    }
                    this.setState({
                        intervalChecker: setInterval(() => this.checkForFeedback(), 1000)
                    })
                })
        }
    }

    checkForFeedback() {
        fetch(this.props.getBackendAddress() + "colang/api/synthesize/status?token=" + this.props.getFeedbackToken(), {
            method: "GET"
        })
            .then((res: any) => {
                return res.json();
            })
            .then((res: any) => {
                if (res.ready) {
                    if (this.state.intervalChecker) {
                        clearInterval(this.state.intervalChecker);
                    }
                    if (res.solved) {
                        this.setState({
                            feedbackRepairs: res.repairs,
                            hasSnoot1: res.snoot1Exists,
                            hasSnoot2: res.snoot2Exists,
                            snoot1: res.snoot1,
                            snoot2: res.snoot2
                        })
                    } else {
                        this.setState({
                            feedbackRepairs: "",
                            hasSnoot1: false,
                            hasSnoot2: false
                        })
                    }
                    this.stopLoading();
                }
            })
    }

    showRepairs() {
        let password = prompt("You need privileged access for this operation! Provide your password below.");
        if (password === ADMIN_PASSWORD) {
            this.setState({
                isShowingRepairs: true
            });
        }
    }

    hideRepairs() {
        this.setState({
            isShowingRepairs: false
        });
    }

    getRepairToggleButton() {
        if (this.state.isShowingRepairs) {
            return (
                <Button variant="secondary" type="submit" className="inline buttonSpacing" onClick={() => this.hideRepairs()}>
                    Hide Repairs
                </Button>
            );
        } else {
            return (
                <Button variant="secondary" type="submit" className="inline buttonSpacing" onClick={() => this.showRepairs()}>
                    Show Repairs
                </Button>
            );
        }
    }

    getRepairsArea() {
        if (this.state.isShowingRepairs) {
            return (
                <Form>
                    <Form.Group style={{ marginTop: "2.25rem" }} controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Repaired Program</Form.Label>
                        <Form.Control disabled as="textarea" rows={20} className="lined code" placeholder="output 'hello world!'" value={this.state.feedbackRepairs} />
                    </Form.Group>
                </Form>
            );
        } else {
            return (
                <></>
            );
        }
    }

    displayFeedback() {
        if (!this.state.isLoading && this.props.getFeedbackToken() !== -1) {
            if (!this.state.hasSnoot1) {
                return (
                    <p>No solutions found!</p>
                );
            } else if (this.state.hasSnoot1 && !this.state.hasSnoot2) {
                return (
                    <p>👃 Something smells funny on line {this.state.snoot1}!</p>
                );
            } else {
                return (
                    <>
                        <p>👃 Something smells funny on line {this.state.snoot1}!</p>
                        <p>👃👃 Something also smells funny on line {this.state.snoot2}!</p>
                    </>
                );
            }
        } else {
            return (
                <p>No feedback yet! Request it to the left.</p>
            );
        }
    }

    startLoading() {
        this.setState({
            isLoading: true
        });
    }

    stopLoading() {
        this.setState({
            isLoading: false
        });
    }

    render() {
        return <div>
            <div>
                <h2 className="inline">Feedback</h2>
                {this.getRepairToggleButton()}
                {this.state.isLoading ? <Spinner className="inline" animation="border" variant="light" /> : <></>}
            </div>
            {this.getRepairsArea()}

            {this.displayFeedback()}
        </div>
    }
}