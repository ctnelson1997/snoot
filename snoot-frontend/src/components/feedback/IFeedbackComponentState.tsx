export default interface IFeedbackComponentState {
    intervalChecker: any,
    localFeedbackToken: number,
    feedbackRepairs: string,
    isShowingRepairs: boolean,
    isLoading: boolean,
    hasSnoot1: boolean,
    hasSnoot2: boolean,
    snoot1: number,
    snoot2: number
}