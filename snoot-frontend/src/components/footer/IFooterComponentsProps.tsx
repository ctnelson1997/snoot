export default interface IFooterComponentProps {
    setBackendAddress: any,
    getBackendAddress: any,
    version: string;
}