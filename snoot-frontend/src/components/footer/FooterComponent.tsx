import './FooterComponent.css';

import React from 'react'
import IFooterComponentProps from './IFooterComponentsProps';

export default class FooterComponent extends React.Component<IFooterComponentProps> {
    render() {
        return <div>

            <footer className="fixed-bottom adjusted-footer">
                <div className="inlineHeader">
                    <h5 style={{ marginLeft: "1rem", marginRight: "-0.5rem", verticalAlign: "bottom" }} className="inlineHeader">Backend Address </h5>
                </div>
                <div className="inlineHeader"  style={{ verticalAlign: "text-bottom", paddingBottom: "0.25rem" }}>
                    <input
                        name="address"
                        style={{ width: "20rem" }}
                        placeholder="backend address"
                        value={this.props.getBackendAddress()}
                        onChange={(e) => this.props.setBackendAddress(e.currentTarget.value)} />
                </div>
                <p>v{this.props.version} | Cole Nelson | ctnelson2@wisc.edu | Built in partial completion for credit in CS703</p>
            </footer>
        </div>
    }
}