import './GraderComponent.css';

import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';

import React from 'react'
import IGraderComponentProps from './IGraderComponentProps';
import IGraderComponentState from './IGraderComponentState';
import { Button, Form, Spinner } from 'react-bootstrap';
import { green, red, grey } from '@material-ui/core/colors';
import { ColangEvaluation } from '../../model/ColangEvaluation';

export default class GraderComponent extends React.Component<IGraderComponentProps, IGraderComponentState> {

    constructor(props: IGraderComponentProps | Readonly<IGraderComponentProps>) {
        super(props);
        this.state = {
            userProgram: "",
            intervalChecker: undefined,
            isLoading: false,
            result: new ColangEvaluation(0, new Date())
        }
    }

    getResults() {
        if (this.state.result.isPassing()) {
            return (
                <>
                    <DoneIcon className="inline" style={{ color: green[500] }} />
                    {this.getDateTimeRepresentation()}
                </>
            );
        } else if (this.state.result.isFailing()) {
            return (
                <>
                    <CloseIcon className="inline" style={{ color: red[500] }} />
                    {this.getDateTimeRepresentation()}
                </>
            );
        } else {
            return (
                <>
                    <HelpOutlineIcon className="inline" style={{ color: grey[500] }} />
                    {this.getDateTimeRepresentation()}
                </>
            );
        }
    }

    getDateTimeRepresentation() {
        return (
            <p className="inline" style={{ verticalAlign: "revert" }}>{"ran on " + this.state.result.getDate().toLocaleDateString() + " " + this.state.result.getDate().toLocaleTimeString()}</p>
        );
    }

    setResult(result: ColangEvaluation) {
        this.setState({
            result: result
        }, () => this.stopLoading())
    }

    evaluate() {
        this.setState({
            isLoading: true
        }, () => this.performEvaluation());
    }

    performEvaluation() {
        fetch(this.props.getBackendAddress() + "colang/api/evaluate", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                content: this.state.userProgram,
                program: this.props.getEnvironment()
            })
        })
            .then((res: any) => {
                return res.json();
            })
            .then((res: any) => {
                this.props.setEvaluationToken(res.token)
                if (this.state.intervalChecker) {
                    clearInterval(this.state.intervalChecker);
                }
                this.setState({
                    intervalChecker: setInterval(() => this.checkEvaluation(), 1000)
                })
            })
    }

    checkEvaluation() {
        fetch(this.props.getBackendAddress() + "colang/api/evaluate/status?token=" + this.props.getEvaluationToken(), {
            method: "GET"
        })
            .then((res: any) => {
                return res.json();
            })
            .then((res: any) => {
                if (res.ready) {
                    if (this.state.intervalChecker) {
                        clearInterval(this.state.intervalChecker);
                    }
                    if (res.correct) {
                        this.setResult(new ColangEvaluation(1, new Date(res.date)));
                    } else {
                        this.setResult(new ColangEvaluation(-1, new Date(res.date)));
                    }
                }
            })
    }

    stopLoading() {
        this.setState({
            isLoading: false
        });
    }

    requestFeedback() {
        fetch(this.props.getBackendAddress() + "colang/api/synthesize", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                content: this.state.userProgram,
                program: this.props.getEnvironment()
            })
        })
            .then((res: any) => {
                return res.json();
            })
            .then((res: any) => {
                this.props.setFeedbackToken(res.token)
            })
    }

    render() {
        return <div>
            <div>
                <h2 className="inline">Submission</h2>
                {
                    this.state.isLoading ? <Spinner className="inline" animation="border" variant="light" /> : <></>
                }
            </div>

            <p>Submit your program below. It may take several seconds to synthesize your feedback, so please be patient.</p>
            <Form>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Enter your program here...</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={20}
                        className="lined code"
                        placeholder="output 'hello world!'"
                        value={this.state.userProgram}
                        onChange={e => this.setState({ userProgram: e.target.value })}
                    />
                </Form.Group>
            </Form>
            <div>
                <Button variant="primary" type="submit" className="buttonSpacing" onClick={() => this.evaluate()}>
                    Submit Program
                </Button>
                <Button variant="secondary" type="submit" className="buttonSpacing" onClick={() => this.requestFeedback()}>
                    Request Feedback
                </Button>
                {this.getResults()}
            </div>
        </div>
    }
}