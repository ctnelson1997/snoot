export default interface IGraderComponentProps {
    setFeedbackToken: any,
    getEvaluationToken: any,
    setEvaluationToken: any,
    getBackendAddress: any,
    getEnvironment: any,
    setEnvironment: any
}