import { ColangEvaluation } from "../../model/ColangEvaluation";

export default interface IGraderComponentState {
    userProgram: string,
    intervalChecker: any,
    isLoading: boolean,
    result: ColangEvaluation
}