import './HeaderComponent.css';

import React from 'react'
import IHeaderComponentProps from './IHeaderComponentProps';

export default class HeaderComponent extends React.Component<IHeaderComponentProps> {

    render() {
        let button = this.props.collapsible()
        return <div>
            <header >
                <h1 className="inlineHeader">Snoot</h1>
                <p className="inlineHeader">An automated grading tool providing feedback through program synthesis.</p>
                <div className="inlineHeader" style={{ verticalAlign: "text-bottom" }}>
                    {button}
                </div>
            </header>
        </div>
    }
}