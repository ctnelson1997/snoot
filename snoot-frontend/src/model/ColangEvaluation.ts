export class ColangEvaluation {

    private result: number;
    private date: Date;

    public constructor(result: number, date: Date) {
        this.result = result;
        this.date = date;
    }

    public getDate(): Date {
        return this.date;
    }

    public isPassing(): boolean {
        return this.result > 0;
    }

    public isFailing(): boolean {
        return this.result < 0;
    }

    public isNotRun(): boolean {
        return this.result === 0;
    }
}