import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';

import FooterComponent from './components/footer/FooterComponent';
import GraderComponent from './components/grader/GraderComponent';
import HeaderComponent from './components/header/HeaderComponent';
import FeedbackComponent from './components/feedback/FeedbackComponent';
import EnvironmentComponent from './components/sidebar/EnvironmentComponent';
import AppState from './AppState';
import AppProps from './AppProps';



const VERSION = "0.1.0";

export default class App extends React.Component<AppProps, AppState> {

  constructor(props: AppProps | Readonly<AppProps>) {
    super(props);
    this.state = {
      feedbackToken: -1,
      evaluationToken: -1,
      backendAddress: "http://localhost:53705/",
      isCollapsed: true,
      environment: "factorial"
    }

    this.getCollapsible = this.getCollapsible.bind(this);
    this.setEnvironment = this.setEnvironment.bind(this);
    this.getEnvironment = this.getEnvironment.bind(this);
    this.setBackendAddress = this.setBackendAddress.bind(this);
    this.getBackendAddress = this.getBackendAddress.bind(this);
    this.setFeedbackToken = this.setFeedbackToken.bind(this);
    this.getFeedbackToken = this.getFeedbackToken.bind(this);
    this.setEvaluationToken = this.setEvaluationToken.bind(this);
    this.getEvaluationToken = this.getEvaluationToken.bind(this);
  }

  setBackendAddress(backendAddress: string) {
    this.setState({
      backendAddress: backendAddress
    });
  }

  getBackendAddress(): string {
    return this.state.backendAddress;
  }

  setEnvironment(environment: string) {
    this.setState({
      environment: environment
    });
  }

  getEnvironment(): string {
    return this.state.environment
  }

  setFeedbackToken(feedbackToken: number) {
    this.setState({
      feedbackToken: feedbackToken
    });
  }

  getFeedbackToken(): number {
    return this.state.feedbackToken;
  }

  setEvaluationToken(evaluationToken: number) {
    this.setState({
      evaluationToken: evaluationToken
    });
  }

  getEvaluationToken(): number {
    return this.state.evaluationToken;
  }

  getCollapsible() {
    if (this.state.isCollapsed) {
      return (
        <>
          <Button variant="primary" type="submit" className="buttonSpacing inline" onClick={() => this.setState({ isCollapsed: false })}>
            Show Environments
          </Button>
        </>
      )
    } else {
      return (
        <>
          <Button variant="primary" type="submit" className="buttonSpacing" onClick={() => this.setState({ isCollapsed: true })}>
            Hide Environments
          </Button>
        </>
      )
    }
  }

  render() {
    return (
      <div>
        <Row className="headerSpacer">
          <HeaderComponent
            collapsible={this.getCollapsible}
          />
        </Row>

        <Row>
          <EnvironmentComponent
            isCollapsed={this.state.isCollapsed}
            getEnvironment={this.getEnvironment}
            setEnvironment={this.setEnvironment}
          />
        </Row>

        <Row>
          <Col lg={6} className="paddedCol">
            <GraderComponent
              setFeedbackToken={this.setFeedbackToken}
              getEvaluationToken={this.getEvaluationToken}
              setEvaluationToken={this.setEvaluationToken}
              getEnvironment={this.getEnvironment}
              setEnvironment={this.setEnvironment}
              getBackendAddress={this.getBackendAddress}
            />
          </Col>

          <Col lg={5} className="paddedCol">
            <FeedbackComponent
              getBackendAddress={this.getBackendAddress}
              getFeedbackToken={this.getFeedbackToken}
            />
          </Col>
        </Row>

        <Row>
          <FooterComponent
            getBackendAddress={this.getBackendAddress}
            setBackendAddress={this.setBackendAddress}
            version={VERSION}
          />
        </Row>
      </div>
    );
  }

}