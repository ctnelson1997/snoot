export default interface AppState {
    backendAddress: string,
    isCollapsed: boolean,
    environment: string,
    feedbackToken: number,
    evaluationToken: number
}