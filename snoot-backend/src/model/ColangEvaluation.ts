export class ColangEvaluation {

    private correct: boolean;
    private date: Date;

    public constructor(correct: boolean, date: Date) {
        this.correct = correct;
        this.date = date;
    }

    public isCorrect(): boolean {
        return this.correct;
    }

    public getDate(): Date {
        return this.date;
    }
}