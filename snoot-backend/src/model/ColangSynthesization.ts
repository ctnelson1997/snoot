export class ColangSynthesization {

    private solved: boolean;
    private date: Date;
    private repairs: string;
    private snoot1Exists: boolean;
    private snoot2Exists: boolean;
    private snoot1: number;
    private snoot2: number;

    public constructor(solved: boolean, date: Date, repairs: string, snoot1Exists: boolean, snoot2Exists: boolean,
        snoot1: number, snoot2: number) {
        this.solved = solved;
        this.date = date;
        this.repairs = repairs;
        this.snoot1Exists = snoot1Exists;
        this.snoot2Exists = snoot2Exists;
        this.snoot1 = snoot1;
        this.snoot2 = snoot2;
    }

    public isSolved(): boolean {
        return this.solved;
    }

    public getDate(): Date {
        return this.date;
    }

    public getRepairs(): string {
        return this.repairs;
    }

    public hasSnoot1(): boolean {
        return this.snoot1Exists;
    }

    public hasSnoot2(): boolean {
        return this.snoot2Exists;
    }

    public getSnoot1(): number {
        return this.snoot1;
    }

    public getSnoot2(): number {
        return this.snoot2;
    }
}