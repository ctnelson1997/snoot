import { ColangEvaluation } from "./model/ColangEvaluation";
import { ColangSynthesization } from "./model/ColangSynthesization";

const express = require('express')
const bodyParser = require('body-parser');
const app = express()
const port = 53705

const exec = require('child_process').exec;
const fs = require('fs');

const TESTS = [
    {
        name: "factorial",
        numTests: 4
    },
    {
        name: "starcase",
        numTests: 4
    }
]

let currentEvaluationToken = 0;
let currentSynthesisToken = 0;

let synthesisTokenMapping: Map<number, ColangSynthesization> = new Map();
let evaluationTokenMapping: Map<number, ColangEvaluation> = new Map();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(function (req: any, res: any, next: any) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/colang/api/evaluate', (req: any, res: any) => {

    let content = req.body.content;
    let program = req.body.program;
    let numTests = getNumberOfTests(program);

    let evaluationToken = getEvaluationToken();
    console.log("RECIEVED")
    console.log(content)
    startSubmission(evaluationToken, content, program, numTests, true);

    res.status(200);
    let responseBody = {
        token: evaluationToken
    }
    res.send(responseBody);
})

app.post('/colang/api/synthesize', (req: any, res: any) => {

    let content = req.body.content;
    let program = req.body.program;
    let numTests = getNumberOfTests(program);

    let synthesisToken = getSynthesisToken();
    startSubmission(synthesisToken, content, program, numTests, false);

    res.status(200);
    let responseBody = {
        token: synthesisToken
    }
    res.send(responseBody);
})

app.get('/colang/api/evaluate/status', (req: any, res: any) => {
    console.log(req.body);

    let responseBody = {
        ready: false,
        correct: false,
        date: new Date()
    };

    if (req.query.token) {
        let result = evaluationTokenMapping.get(parseInt(req.query.token));
        if (result !== undefined) {
            responseBody = {
                ready: true,
                correct: result.isCorrect(),
                date: result.getDate()
            }
        }
    }

    res.status(200);
    res.send(responseBody);
})

app.get('/colang/api/synthesize/status', (req: any, res: any) => {
    console.log(req.body);

    let responseBody = {
        ready: false,
        solved: false,
        date: new Date(),
        repairs: "",
        snoot1Exists: false,
        snoot2Exists: false,
        snoot1: 0,
        snoot2: 0
    };

    if (req.query.token) {
        let result = synthesisTokenMapping.get(parseInt(req.query.token));
        if (result !== undefined) {
            responseBody = {
                ready: true,
                solved: result.isSolved(),
                date: result.getDate(),
                repairs: result.getRepairs(),
                snoot1Exists: result.hasSnoot1(),
                snoot2Exists: result.hasSnoot2(),
                snoot1: result.getSnoot1(),
                snoot2: result.getSnoot2()
            };
        }
    }

    res.status(200);
    res.send(responseBody);
})

app.listen(port, () => {
    console.log(`Snoot-Backend running on :${port}`)
})

function getEvaluationToken() {
    return currentEvaluationToken++;
}

function getSynthesisToken() {
    return currentSynthesisToken++;
}

function getNumberOfTests(program: string): number {
    let numTests = 0;
    TESTS.forEach((test: any) => {
        if (test.name === program) {
            numTests = test.numTests;
        }
    });
    return numTests;
}

function startSubmission(token: number, content: string, program: string, numTests: number, isEvaluation: boolean) {
    let name = getColangFolderName()
    let path = "./filesys/submissions/"
    exec("cd " + path + " && " +
        "mkdir " + name + " && " +
        "cd " + name + " && " +
        "mkdir in && mkdir out && mkdir work",
        function (error: any, stdout: any, stderr: any) {
            if (error) {
                console.error(stderr)
            } else {
                fs.writeFile(path + name + '/in/ColangProgram.colang', content, function (err: any) {
                    if (err) {
                        return console.error(err);
                    } else {
                        if (isEvaluation) {
                            doEvaluation(token, path + name, "./filesys/programs/" + program + "/tests/", numTests)
                        } else {
                            doSynthesization(token, path + name, "./filesys/programs/" + program + "/tests/", numTests)
                        }
                        console.log(stdout)
                    }
                });
            }
        });
}

function doEvaluation(evaluationToken: number, path: string, tests: string, numTests: number) {
    let command = "colangopt evaluate " +
        `in="${path}/in/ColangProgram.colang" ` +
        `out="${path}/out/" ` +
        `work="${path}/work/" ` +
        `tests="${tests}" ` +
        `numTests=${numTests}`;
    console.log(`Executing ${command}`)

    exec(command, function (error: any, stdout: any, stderr: any) {
        if (error) {
            console.error(stderr)
        } else {
            evaluationTokenMapping.set(evaluationToken, new ColangEvaluation((stdout.trim() === 'true'), new Date()));
        }
    });
}

function doSynthesization(synthesisToken: number, path: string, tests: string, numTests: number) {
    let command = "colangopt synthesize " +
        `in="${path}/in/ColangProgram.colang" ` +
        `out="${path}/out/" ` +
        `work="${path}/work/" ` +
        `tests="${tests}" ` +
        `numTests=${numTests}`;
    console.log(`Executing ${command}`)

    exec(command, function (error: any, stdout: any, stderr: any) {
        if (error) {
            console.error(stderr)
        } else {
            console.log(stdout);
            readSynthesisResults(synthesisToken, `${path}/out/`)
        }
    });
}

function readSynthesisResults(synthesisToken: number, path: string) {
    fs.readFile(path + 'ColangProgram.colang', "utf8", function read(err: any, data: any) {
        if (err) {
            console.error(err)
        } else {
            readRepairsResults(synthesisToken, path, data);
        }
    });
}

function readRepairsResults(synthesisToken: number, path: string, repairedProgram: string) {
    fs.readFile(path + 'Repairs.json', "utf8", function read(err: any, data: any) {
        if (err) {
            console.error(err)
        } else {
            let repairs = JSON.parse(data);
            let snoot1 = repairs.repair1 ? repairs.repair1 : 0;
            let snoot2 = repairs.repair2 ? repairs.repair2 : 0;
            let hasSnoot1 = repairs.repair1 !== undefined;
            let hasSnoot2 = repairs.repair2 !== undefined;
            synthesisTokenMapping.set(synthesisToken, new ColangSynthesization(true, new Date(), repairedProgram, hasSnoot1, hasSnoot2, snoot1, snoot2));
        }
    });
}

function getColangFolderName() {
    let today = new Date();
    let y = today.getFullYear();
    let m = today.getMonth() + 1;
    let d = today.getDate();
    let h = today.getHours();
    let mi = today.getMinutes();
    let s = today.getSeconds();
    return "colang" + "_" + y + "_" + m + "_" + d + "_" + h + "_" + mi + "_" + s;
}

// startSubmission("outputln 'hello world'", "factorial", true);
